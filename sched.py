#!/usr/bin/env python
#encoding: utf-8
import requests
import logging
import kiraiot
import time
import datetime
import config
import MySQLdb
import demjson
import sys
import xlsxwriter
import schedule
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from kiraiot import KiraClient,Action

reload(sys)
sys.setdefaultencoding("utf-8")
# create logger with 'spam_application'
logger = logging.getLogger('mailsystem_application')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('mailsystem.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)


def notify(text, desp):
    sendkey = '940-8aa800fa594e4689d11e86fe66b7391e'
    url = "https://pushbear.ftqq.com/sub?sendkey={}&text={}&desp={}".format(sendkey,text, desp)
    res = requests.get(url)
    logger.info(res)


def send_email(to,title, input_msg, file_name=""):
    msg = MIMEMultipart()
    msg["Subject"] = title
    msg["From"]    = config.user
    msg["To"]      = ','.join(to)
    print "准备发送邮件"
    part = MIMEText(str(input_msg), 'html', 'utf-8')
    msg.attach(part)
    if file_name != "":
        part = MIMEApplication(open(str(file_name),'rb').read())
        part.add_header('Content-Disposition', 'attachment', filename=str(file_name))
        msg.attach(part)
    s = smtplib.SMTP_SSL(config.smtp, config.port)#连接smtp邮件服务器,端口默认是25
    s.login(config.user, config.pwd)#登陆服务器
    s.sendmail(config.user, to, msg.as_string())#发送邮件
    s.quit()
    logger.info("邮件发送成功")


def get_con():
    host = "192.168.16.17"
    port = 3306
    logsdb = "testproduct"
    user = "root"
    password = "goodlife1"
    con = MySQLdb.connect(host=host, user=user, passwd=password, db=logsdb, port=port, charset='utf8')
    return con


def calculate_time():
    now = time.mktime(datetime.datetime.now().timetuple())
    result = time.strftime('%Y-%m-%d', time.localtime(now))
    return result


def data_to_excel(result,fields):
    # 定义时间标志变量
    datetime.timedelta()
    now_time = datetime.datetime.now()
    yes_time = now_time + datetime.timedelta(days=-1)
    book_mark = str(yes_time.strftime('%Y-%m-%d')).encode('utf-8')
    # 定义输出excel文件名
    workbook = xlsxwriter.Workbook(book_mark+'.xlsx')
    # 定义sheet的名字
    worksheet = workbook.add_worksheet(book_mark)
    # 定义sheet中title的字体format
    bold = workbook.add_format({'bold': True})
    for field in range(0,len(fields)):
        worksheet.write(0,field,fields[field][0],bold)
    for row in range(1,len(result)+1):
        for col in range(0,len(fields)):
            worksheet.write(row,col,u'%s' % str(result[row-1][col]))
    return book_mark


def operateTask():
    sql="SELECT user_name,operate_message FROM operation where operate_event = 'updateProduct' and operate_message is not null and date(operate_time) = DATE_ADD(CURDATE(), INTERVAL -1 DAY)"
    conn = get_con()               # 和数据库之间建立联系，
    cursor = conn.cursor()         # 创建可以直接操作数据库的对象
    cursor.execute(sql)
    results = cursor.fetchall()        # 获取到相应的结果
    total = 0
    content = '<table border="1"><tr><th>处理人</th><th>详细信息</th><tr>'
    if results is not None and len(results):
        for r in results:
            total+= 1
            content =content+"<tr><td>"+r[0].encode("utf-8")+"</td><td>"+r[1].encode("utf-8")+"</td></tr>"
        content = "<h1>"+str(datetime.datetime.now().strftime('%Y-%m-%d')).encode('utf-8')+"共修改数据"+str(total)+"条,详细见下表:</h1>"+content+"</table>"+"<h3>请知悉！</h3>"
        datetime.timedelta()
        now_time = datetime.datetime.now()
        yes_time = now_time + datetime.timedelta(days=-1)
        title = str(yes_time.strftime('%Y-%m-%d')).encode('utf-8')+'修改信息统计数据'
        try:
            send_email(config.operateTo,title, content)
            notify(str(title),"修改信息邮件已经发送请及时查收")
        except Exception,e:
            logger.error(e)
            logger.info('网络中止')
    else:
        time = datetime.datetime.now()
        notify(str(time),"昨日没有修改数据")
    cursor.close()
    conn.close()
    logger.info('修改数据邮件发送完成')


def markTask():
    sql = "SELECT order_pi, count(order_pi) as tatal from device where date(order_input_time)=DATE_ADD(CURDATE(), INTERVAL -1 DAY) group by order_pi"
    conn = get_con()
    cursor = conn.cursor()
    cursor.execute(sql)
    results = cursor.fetchall()           # 以上代码就是为了获取到结果
    fields = cursor.description
    logger.info("数据的长度为{} 数据是：{}".format(len(results),results))
    try:
        if results is not None and len(results):
            total = 0
            content = '<table border="1"><tr><th>company</th><th>count</th><tr>'
            datetime.timedelta()
            now_time = datetime.datetime.now()
            yes_time = now_time + datetime.timedelta(days=-1)
            for r in results:
                total+= int(r[1])
                content =content+"<tr><td>"+str(r[0]).encode("utf-8")+"</td><td>"+str(r[1]).encode("utf-8")+"</td></tr>"
            content = "<h1>"+str(yes_time.strftime('%Y-%m-%d')).encode('utf-8')+"共入库"+str(total)+"套,其中:</h1>"+content+"</table>"+"<h3>详细MAC信息在附件的excel中。</h3>"
            e_data = "SELECT order_pi, mac_address, order_input_time from device where date(order_input_time)=DATE_ADD(CURDATE(), INTERVAL -1 DAY)"
            cursor.execute(e_data)
            results = cursor.fetchall()
            fields = cursor.description
            book_mark = data_to_excel(results, fields)+'.xlsx'
            title = str(yes_time.strftime('%Y-%m-%d')).encode('utf-8')+'入库信息统计数据'
            try:
                send_email(config.markTo, title, content, book_mark)
                notify(str(title),"入库信息邮件已经发送请及时查收")
            except Exception,e:
                logger.error(e)
                logger.info('网络中止')
        else:
            time = datetime.datetime.now()
            notify(str(time),"昨天日没有入库数据")
    except Exception, e:
        time = datetime.datetime.now()
        notify(str(time),"发生了错误")
        logger.error(e)
    cursor.close()
    conn.close()
    logger.info("入库数据邮件发送完成")


if __name__ == '__main__':
    client = KiraClient(("192.168.16.17", 1883), 'gliot', '192.168.16.17')    # 建立这个连接对象，
    client.start()        # 物联网不用关心
    schedule.every().day.at(config.markAutoTime).do(markTask).tag('markAuto')
    schedule.every().day.at(config.operateAutoTime).do(operateTask).tag('operateAuto')


    @client.route("onlive")
    def handleOneLiveAction(client, action):
        logger.info("handleOneLiveAction()")
        notify(str(datetime.datetime.now())+"handleOneLiveAction()", action.sender+"onlive")
        data = demjson.encode({'status': 1})
        actionReply = Action.buildReplyAction(action, "onlive",data)
        client.sendAction(actionReply)


    @client.route("userInfo")
    def handleUserName(client,action):
        logger.info("handleUserName()")
        try:
            config.user = action.user
            config.pwd = action.pwd
            config.smtp = action.smtp
            config.port = int(action.port)
        except:
            config.user = "huyiyong@gl-inet.com"
            config.pwd  = "goodLife1"
            config.smtp = "smtp.exmail.qq.com"
            config.port = 465
        try:
            send_email(['phunsukewonder@outlook.com'],'测试邮件','测试邮件')
            data = demjson.encode({'status': 1})
            actionReply = Action.buildReplyAction(action, "userInfo",data)
            client.sendAction(actionReply)
        except:
            data = demjson.encode({'status': 0})
            actionReply = Action.buildReplyAction(action, "userInfo",data)
            client.sendAction(actionReply)


    @client.route('getInfo')
    def handleGetInfo(client,action):
        logger.info("handleGetInfo()")
        data = demjson.encode({'status': 1,'user':config.user,'smtp':config.smtp,'port': config.port,'markTo':config.markTo,'operateTo':config.operateTo,'markAutoTime':config.markAutoTime,'operateAutoTime':config.operateAutoTime})
        actionReply = Action.buildReplyAction(action, "getInfo",data)
        client.sendAction(actionReply)


    @client.route("markAuto")
    def handlemarkAutoAction(client, action):
        logger.info("handlemarkAutoAction()")
        try:
            schedule.clear('markAuto')
        except:
            pass
        try:
            config.markAutoTime = action.autoTime
        except:
            config.markAutoTime = "21:58"
        schedule.every().day.at(config.markAutoTime).do(markTask).tag('markAuto')
        logger.info("schedule every data at {}, userlist {}".format(config.markAutoTime,config.markTo))
        data = demjson.encode({'status': 1,"autoTime": config.markAutoTime})
        actionReply = Action.buildReplyAction(action, "markAuto",data)
        client.sendAction(actionReply)


    @client.route("operateAuto")
    def handleoperateAutoAction(client, action):
        logger.info("handleoperateAutoAction()")
        try:
            schedule.clear('operateAuto')
        except:
            pass
        try:
            config.operateAutoTime = action.autoTime
        except:
            config.operateAutoTime = "21:55"
        schedule.every().day.at(config.operateAutoTime).do(operateTask).tag('operateAuto')
        logger.info("schedule every data at {}, userlist {}".format(config.operateAutoTime,config.operateTo))
        data = demjson.encode({'status': 1,"autoTime": config.operateAutoTime})
        actionReply = Action.buildReplyAction(action, "operateAuto",data)
        client.sendAction(actionReply)


    @client.route("markUser")
    def handlemarkUserAction(client, action):
        logger.info("handlemarkUserAction()")
        try:
            config.markTo = action.userlist
        except:
            pass
        data = demjson.encode({'status': 1, "userlist": config.markTo})
        actionReply = Action.buildReplyAction(action, "userlist",data)
        client.sendAction(actionReply)


    @client.route("operateUser")
    def handleoperateUserAction(client, action):
        logger.info("handleoperateUserAction()")
        try:
            config.operateTo = action.userlist
        except:
            pass
        data = demjson.encode({'status': 1, "userlist": config.operateTo})
        actionReply = Action.buildReplyAction(action, "userlist",data)
        client.sendAction(actionReply)
    while True:
         schedule.run_pending()
         time.sleep(1)
