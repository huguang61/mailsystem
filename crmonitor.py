# -*- coding: utf-8 -*-
import MySQLdb
import datetime
import time
from mail import email as send_email
import sys
import xlsxwriter

def get_con():
    host = "192.168.16.17"
    port = 3306
    logsdb = "ddns_mac_mark_company"
    user = "root"
    password = "goodlife1"
    con = MySQLdb.connect(host=host, user=user, passwd=password, db=logsdb, port=port, charset='utf8')
    return con

def data_to_excel(result,fields):
    # 定义时间标志变量
    sheet_time = datetime.datetime.now()
    sheet_mark = sheet_time.strftime('%Y-%m-%d')
    book_mark = sheet_time.strftime('%Y%m%d')
    # 定义输出excel文件名
    workbook = xlsxwriter.Workbook(book_mark+'.xlsx')
    # 定义sheet的名字
    worksheet = workbook.add_worksheet(sheet_mark)
    # 定义sheet中title的字体format
    bold = workbook.add_format({'bold': True})
    for field in range(0,len(fields)):
        worksheet.write(0,field,fields[field][0],bold)
    for row in range(1,len(result)+1):
        for col in range(0,len(fields)):
            worksheet.write(row,col,u'%s' % str(result[row-1][col]))
    print "写入数据到excel完成"
    return book_mark

def task():
    sql = "SELECT company, count(company) as tatal from ddns_mac_mark_company.ddns where date(delivery_time)= 'curdate()' group by company"
    conn = get_con()
    cursor = conn.cursor()
    cursor.execute(sql)
    results = cursor.fetchall()
    fields = cursor.description
    print len(results)
    if results is not None and len(results) > 1:
        print "读取数据完成"
        total = 0
        content = '<table border="1"><tr><th>company</th><th>count</th><tr>'
        for r in results:
            total+= int(r[1])
            content =content+"<tr><td>"+str(r[0]).encode("utf-8")+"</td><td>"+str(r[1]).encode("utf-8")+"</td></tr>"
        content = "<h1>"+datetime.datetime.now().strftime('%Y-%m-%d')+"共入库"+str(total)+"套,其中:</h1>"+content+"</table>"+"<h3>详细MAC信息在附件的excel中。</h3>"
        e_data = "SELECT company, mac_address, delivery_time from ddns_mac_mark_company.ddns where date(delivery_time)=curdate()"
        cursor.execute(e_data)
        results = cursor.fetchall()
        fields = cursor.description
        book_mark = data_to_excel(results, fields)+'.xlsx'
        title = datetime.datetime.now().strftime('%Y-%m-%d')
        send_email(title, content, book_mark)
        cursor.close()
        conn.close()
    print "完成"

if __name__ == "__main__":
    task()
