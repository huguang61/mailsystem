#!/usr/bin/env python
#encoding: utf-8
import MySQLdb
import datetime
import smtplib
import logging
import sys
import config
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

reload(sys)
sys.setdefaultencoding("utf-8")
# create logger with 'spam_application'
logger = logging.getLogger('mailsystem_application')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('mailsystem.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

def get_con():
    host = "192.168.16.17"
    port = 3306
    logsdb = "testproduct"
    user = "root"
    password = "goodlife1"
    con = MySQLdb.connect(host=host, user=user, passwd=password, db=logsdb, port=port, charset='utf8')
    return con

def send_email(to,title, input_msg, file_name=""):
    msg = MIMEMultipart()
    msg["Subject"] = title
    msg["From"] = config.user
    msg["To"] = ','.join(to)
    print "准备发送邮件"
    part = MIMEText(str(input_msg), 'html', 'utf-8')
    msg.attach(part)
    if file_name != "":
        part = MIMEApplication(open(str(file_name),'rb').read())
        part.add_header('Content-Disposition', 'attachment', filename=str(file_name))
        msg.attach(part)
    s = smtplib.SMTP_SSL("smtp.exmail.qq.com", 465)#连接smtp邮件服务器,端口默认是25
    s.login(config.user, config.pwd)#登陆服务器
    s.sendmail(config.user, to, msg.as_string())#发送邮件
    s.quit()
    logger.info("邮件发送成功")

def operateTask():
    sql="SELECT user_name,operate_message FROM operation where operate_event = 'updateProduct' and operate_message is not null and date(operate_time) = curdate()"
    conn = get_con()
    cursor = conn.cursor()
    cursor.execute(sql)
    results = cursor.fetchall()
    total = 0
    content = '<table border="1"><tr><th>处理人</th><th>详细信息</th><tr>'
    if results is not None and len(results):
        for r in results:
            total+= 1
            content =content+"<tr><td>"+r[0].encode("utf-8")+"</td><td>"+r[1].encode("utf-8")+"</td></tr>"
        content = "<h1>"+str(datetime.datetime.now().strftime('%Y-%m-%d')).encode('utf-8')+"共修改数据"+str(total)+"条,详细见下表:</h1>"+content+"</table>"+"<h3>请知悉！</h3>"
        title = str(datetime.datetime.now().strftime('%Y-%m-%d')).encode('utf-8')+'修改信息统计数据'
        try:
            send_email(config.operateTo,title, content)
            notify(str(title),"修改信息邮件已经发送请及时查收")
        except Exception,e:
            logger.error(e)
            logger.info('网络中止')
    else:
        time = datetime.datetime.now()
        print "没有数据"
        # notify(str(time),"今日没有修改数据")
    cursor.close()
    conn.close()
    logger.info('修改数据邮件发送完成')
operateTask()




