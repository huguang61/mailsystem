#!/usr/bin/env python
#encoding: utf-8

import logging
import time
import sys
import getopt
import random
import string
import requests
import demjson
import socket, fcntl, struct
import config
from paho.mqtt.client import Client as Mqtt

VERSION = "1.0.1"

"""
    1.每个设备在没有绑定之前，都是处于暴露在公共服务状态，只有
    在控制端与其建立了通信连接的时候，这时候，设备和终端处于
    通信独享状态。
    2.
"""
KIRA_BROADCAST_RECEIVER = 'kira_inet' # 广播接受者

logging.basicConfig(level=logging.DEBUG,
    format = '[%(asctime)s] %(levelname)s %(message)s',
    datefmt = '%Y-%m-%d %H:%M:%S')
root = logging.getLogger()
root.setLevel(logging.NOTSET)


class Action(object):
    """行为动作类封装"""
    def __init__(self, receiver, sender, actionName, payload=None):
        self.decode(payload)
        self.receiver = receiver
        self.sender = sender
        self.action = actionName

    def decode(self, payload):
        if payload:
            try:
                json = demjson.decode(payload)
                self.__dict__.update(json)
            except demjson.JSONDecodeError:
                raise ValueError

    def encode(self):
        return demjson.encode(self.__dict__).encode("utf-8")

    @staticmethod
    def buildAction(receiver, sender, actionName, payload=None):
        return Action(receiver, sender, actionName, payload)

    @staticmethod
    def buildReplyAction(action, actionName,payload=None):
        if action:
            return Action.buildAction(action.sender, action.receiver, actionName,payload)

class MqttClient(object):
    """Mqtt通讯封装"""
    def __init__(self, address):
        if not isinstance(address, tuple) or len(address) != 2:
            raise ValueError("Invalid address.")
        def on_connect(client, userdata, flags, rc):
            self.handleConnected()

        def on_message(client, userdata, msg):
            self.handleMessage(msg.topic, msg.payload)

        self.client = Mqtt()
        self.address = address
        self.sendkey = "942-2bb5678e68ea85bc7dc76752cf43334a"
        self.client.on_connect = on_connect
        self.client.on_message = on_message

    def handleConnected(self):
        logging.info("MqttClient.handleConnected()")

    def handleMessage(self, topic, payload):
        logging.info("MqttClient.handleMessage() topic={} payload={}".format(topic,payload))

    def publish(self, topic, payload=None, qos=2, retain=False):
        logging.info("MqttClient.publish() topic={} payload={}".format(topic,payload))
        self.client.publish(topic, payload, qos, retain)

    def subscribe(self, topic, qos=0):
        logging.info("MqttClient.subscribe() topic={}".format(topic))
        self.client.subscribe(topic, qos)

    def sendMessage(self, topic, payload=None, qos=2, retain=False):
        logging.info("MqttClient.sendMessage() topic={}".format(topic))
        self.client.publish(topic, payload, qos, retain)

    def start(self):
        self.client.connect_async(self.address[0],self.address[1],keepalive=13*60*60)
        self.client.loop_start()

    def stop(self):
        logging.info("MqttClient.stop()")
        self.client.loop_stop()

    def notify(self, text, desp):
        url = "https://pushbear.ftqq.com/sub?sendkey={}&text={}&desp={}".format(self.sendkey,text, desp)
        res = requests.get(url)
        logging.info(res)

    def username_pw_set(self, username, password=None):
        self.client.username_pw_set(username, password)

    def will_set(self, topic, payload=None, qos=2, retain=False):
        logging.info("MqttClient.will_set() topic={} payload={}".format(topic, payload))
        self.client.will_set(topic, payload, qos, retain)

class KiraBase(MqttClient):
    """
    Kira物联通信基类
    nodeid 节点号:如果用户有传便是用户所定制的，如果没有传，在可以调用ip地址的情况下为ip地址，如果取不到便是随机码
    accountid 通道号: 如果用户有传便是用户定制，如果没有传，便是默认的kiraiot
    """

    def __init__(self, address, accountid=None, nodeid=None, authkey=None, username=None, password=None):
        """username和password是mqtt账号密码"""
        logging.info("KiraBase.__init__() address=({}, {}), accountid={}, nodeid={}".format(address[0], address[1], accountid, nodeid))
        super(KiraBase, self).__init__(address)
        self.username = username
        self.password = password
        if accountid:
            self.accountid = accountid
        else:
            self.accountid = u'kiraiot'
        if nodeid:
            self.nodeid = nodeid
        else:
            try:
                self.nodeid = self.get_ip2('eth0')
            except:
                self.nodeid = self.activation_code(random.randint(1,225))
        self.authkey = authkey
        self.handlers = {}

    def get_ip2(self, ifname):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])

    def activation_code(self, id, length=10):
        """
        id + L + 随机码
        string模块中的3个函数：string.letters，string.printable，string.printable
        """
        prefix = hex(int(id))[2:]+ 'L'
        length = length - len(prefix)
        chars=string.ascii_letters+string.digits
        return prefix + ''.join([random.choice(chars) for i in range(length)])

    def sendAction(self, action):
        logging.info("KiraBase.sendAction()")
        if action:
            topic = "{self.accountid}/{action.receiver}/{self.nodeid}/{action.action}".format(self=self, action=action)
            payload = action.encode()
            self.sendMessage(topic, payload)

    def handleAction(self, action):
        logging.info("YqmiotClient.handleAction()")
        if action:
            handler = self.handlers.get(action.action)
            if handler:
                handler(self, action)

    def handleConnected(self):
        logging.info("KiraBase.handleConnected()")
        super(KiraBase, self).handleConnected()
        topic = "{self.accountid}/{self.nodeid}/#".format(self=self)
        self.subscribe(topic)

    def handleMessage(self, topic, payload):
        logging.info("KiraBase.handleMessage() topic={} payload={}".format(topic, payload))
        super(KiraBase, self).handleMessage(topic, payload)
        try:
            account,receiver,sender,command = topic.split("/")
            account = str(account)
            receiver = str(receiver)
            sender = str(sender)
        except:
            logging.error("Invalid topic. {}".format(topic))
            return

        action = Action.buildAction(receiver, sender, command)

        if action:
            try:
                action.decode(payload)
            except ValueError:
                logging.info("the payload format invalid. {} {}".format(topic, payload))
                return

            try:
                self.handleAction(action)
            except:
                raise
        else:
            logging.info("the action not found. {}".format(topic))

    def route(self, actionName):
        logging.info("KiraBase.route() actionName=%s" % actionName)
        def decorator(func):
            self.handlers[actionName] = func
            return func
        return decorator

class KiraClient(KiraBase):
    """
        Kira物联客户端
    """

    def start(self):
        # 离线通知
        topic = "{}/{}/{}/{}".format(self.accountid, KIRA_BROADCAST_RECEIVER,self.nodeid, 'offline')
        payload = {"id":self.nodeid, "action": 'offline'}
        self.will_set(topic, demjson.encode(payload))

        super(KiraClient, self).start()

    def handleConnected(self):
        super(KiraClient, self).handleConnected()
        logging.info("Connect server successfully.")

        # 上线通知
        actionOnline = Action.buildAction(KIRA_BROADCAST_RECEIVER,self.nodeid, "online")
        self.sendAction(actionOnline)

if __name__ == '__main__':

    client = KiraClient(("192.168.16.16",1883), 'gliot')
    client.start()
    @client.route("ping")
    def handlePingAction(client, action):
        logging.info("handlePingAction() ")
        if config.authkey is None:
            config.authkey = action.authkey
        data = demjson.encode({'authkey': config.authkey})
        actionReply = Action.buildReplyAction(action, "pong",data)
        client.sendAction(actionReply)

    while True:
        pass
